#!/usr/bin/python3

from HomeWork import homeWork
from HomeWork import faceSave
from HomeWork import trainingDataset
import os
import sys


bold = '\033[1m '
end = '\033[0m'

recognizerPath = 'trainner/trainner.yml'
cascadePath = '/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml'

print("####################################################################")
print("                   PROJECT-SECURITY CAMERA")
print("####################################################################")
print("To " + bold + " save " + end + " faces type 'save' ")
print("To " + bold + "train " + end + " faces type 'training' or train ")
print("To enable " + bold + "security camera " + end + " function type 'camera' or 'cam' ")
print("To " + bold + "exit " + end + " application type 'exit' or press 'q'")

while True:
    sel = input("\ntype to select: \n")
    if sel == 'camera' or sel == 'cam':
        homeWork.securityCam(1, recognizerPath, cascadePath)
    elif sel == 'save':
        faceSave.faceSave(1, cascadePath)
    elif sel == 'training' or sel == 'train':
        trainingDataset.trainingDataSet(cascadePath)
    if sel == 'exit' or sel == 'q':
        break

    #sp.call('clear', shell=True)
    sys.stdout.write("\x1b[2J\x1b[H")
    os.system("clear")

    print("To " + bold + " save " + end + " faces type 'save' ")
    print("To " + bold + "train " + end + " faces type 'training' ")
    print("To enable " + bold + "security camera " + end + " function type 'camera'")
    print("To " + bold + "exit " + end + " application type 'exit' or press 'q'")

print("Bye")