import cv2


def faceSave(camchannel, cascadePath):
    cam = cv2.VideoCapture(camchannel)
    detector = cv2.CascadeClassifier(cascadePath)
    Id = input('enter your id: ')
    sum_num = 11
    sampleNum = 0
    print("Press 'n' to capture next picture")
    print("Press 1 sec 'Esc' to exit or take 12 pictures")
    cv2.namedWindow("img")
    while (True):
        ret, img = cam.read()
        if not ret:
            continue

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = detector.detectMultiScale(gray, 1.3, 5)
        #cv2.imshow("img", img)
        #print(faces)
        if(len(faces) > 0):
            (x, y, w, h) = faces[0]
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        if(cv2.waitKey(1) & 0xFF  == ord('n') and len(faces) > 0):
            # saving the captured face in the dataset folder
            print("save")
            cv2.imwrite("dataSet/User." + str(Id) + '.' + str(sampleNum) + ".jpg", gray[y:y + h, x:x + w])
            print("Picture saved. There is " + str(sum_num - sampleNum) + " picture left to capture.")
            sampleNum = sampleNum + 1


        cv2.imshow("img", img)
        k = cv2.waitKey(1) & 0xFF
        if sampleNum > sum_num or k == 27:
            break

    cam.release()
    cv2.destroyAllWindows()

#cascadePath = '/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml'

#faceSave(1, cascadePath)
