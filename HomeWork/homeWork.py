#!/usr/bin/python3

import numpy as np
import cv2
#import datetime
import time
#from datetime import datetime

camchannel = 0

def nothing(x):
    pass

##############################################################
##              FILE OPERATIONS
##############################################################
def getId (path):
    file = open(path, "r+")
    position = file.seek(0, 0)
    id = int(file.read(2))
    file.close()
    return id

def saveId(id, path):
    file = open(path, "r+")
    position = file.seek(0, 0)
    file.write(str(id))
    file.close()

#################################################################
##                  CREATE TRACKBARS
#################################################################
def createTrackbars(windowname):
    """ This function creates trackbar for treshold minimum and
    maximumvalue, erode an dilate (how much iterations) and one trackbar
    for multiplyvalue to the grayscale diffimage"""
    im = np.zeros((250,300,1),np.uint8)
    cv2.namedWindow(windowname)
    cv2.createTrackbar('min', windowname, 0, 255, nothing)
    cv2.createTrackbar('max', windowname, 0, 255, nothing)
    cv2.createTrackbar('erode', windowname, 0, 100, nothing)
    cv2.createTrackbar('dilate', windowname, 0, 100, nothing)
    cv2.createTrackbar('width_min', windowname, 0, 800, nothing)
    cv2.createTrackbar('height_min', windowname, 0, 600, nothing)
    cv2.createTrackbar('multiply', windowname,0,50,nothing)
    cv2.imshow(windowname,im)

    # set default parameters
    cv2.setTrackbarPos('max', windowname, 255)
    cv2.setTrackbarPos('min', windowname, 20)
    cv2.setTrackbarPos('dilate', windowname, 12)
    cv2.setTrackbarPos('multiply', windowname, 2) #changed
    cv2.setTrackbarPos('width_min', windowname, 125) #changed
    cv2.setTrackbarPos('height_min', windowname, 1)
    return im


###########################################################################
#                   READ FRAME
###########################################################################
def readFrame(camStream):
    """reads frame from camera and converts to grayscael image
        flag is true if read was succesful
        frame is the original frame
        gray is the grayscaled image"""
    flag, frame = camStream.read()
    if (not flag):
        print('frame1 could not read\n')
    frame = cv2.medianBlur(frame, 5)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return flag, frame, gray

###########################################################################
#                     DRAW BOUNDING BOX
###########################################################################
def drawBoundingBox(contours,frame):
    """draws a boundingbox in frame"""
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if ( w < minWidth or h < minHeight) :
            continue
        # print('width: ' + str(w) + '  height: ' + str(h) + '\n')
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
    return


def initRecognizer(recognizerPath):
    recognizer = cv2.face.createLBPHFaceRecognizer()
    recognizer.load(recognizerPath)
    #faceCascade = cv2.CascadeClassifier(cascadePath)
    return recognizer

def faceRecognition(faces, frame, gray,  numberOfSamples, saveImage, recognizer):
    Id = -1
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 0), 2)
        roi_color = frame[y:y + h, x:x + w]
        Id, conf = recognizer.predict(gray[y:y + h, x:x + w])
        if (conf > 110):
            Id = -1
       # print("id: " + str(Id))
        if len(faces) > 0 and saveImage and Id == -1 :
            print('saving\n')
            numberOfSamples += 1
            #cv2.imwrite('savedImages/' + str(datetime.now()) + '.jpg', roi_color)
            cv2.imwrite("dataSet/User." + str(ID) + '.' + str(numPictures) + ".jpg", gray[y:y + h, x:x + w])
            prevtime = curtime
    return numberOfSamples, Id


###########################################################################
#                    SECURITY CAMERA FUNCTION
###########################################################################
def securityCam(camChannel, recognizerPath, cascadePath):
    global camchannel
    global numPictures
    global ID
    global prevtime
    global curtime
    global minWidth
    global minHeight

    print('Starting....')
    path = "id.txt"
    print("Opening File with ID... ")
    print("Press 's' to activate / deactivate saving mode")
    #print("Press 'd' to activate / debug mode ")
    print("Press 'Esc' to exit security cam mode")

    ID = getId(path)
    ID += 1

    numPictures = 0

    state = ' '
    prevstate = state
    face_state=' '
    saveImage = False
    prevDebug = 0
    recognizer = initRecognizer(recognizerPath)
    face_cascade = cv2.CascadeClassifier(cascadePath)


    # create trackbars for tuning the parameters
    blank = createTrackbars('track')
    print('Opening camera stream...')
    cap = cv2.VideoCapture(camChannel)  # 1 is webcam, 0 is pc_cam
    time.sleep(0.25)
    print("Succes")

    # set resolution
    cap.set(3, 600)
    cap.set(4, 800)

    prevtime = time.time()
    curtime = prevtime
    cikltime = time.time()
    prevcikltime = cikltime
    # if debug == 1 shows up extra windows and infos
    debug = 0

    i = 0
    st = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
    ##################################################################
    #                       MAINLOOP
    ##################################################################
    while True:
        #id = "    "
        #read the first frame
        ret1, frame1, gray1 = readFrame(cap)
        if(not ret1):
            continue

        #read the second frame
        ret2, frame2, gray2 = readFrame(cap)
        if not ret2:
            continue
        i += 1

        # get difference of the two pictures;
        diff = cv2.absdiff(gray1, gray2)
        multiply_const = cv2.getTrackbarPos('multiply', 'track')
        diff = cv2.multiply(diff,multiply_const)  #multiply the grayscaled picture

        mint = cv2.getTrackbarPos('min', 'track') #min reshold
        maxt = cv2.getTrackbarPos('max', 'track') #max treshold
        eroditer = cv2.getTrackbarPos('erode', 'track')
        dilatiter = cv2.getTrackbarPos('dilate', 'track')
        minWidth = cv2.getTrackbarPos('width_min','track')
        minHeight = cv2.getTrackbarPos('height_min','track')

        #treshold the differenceimage
        ret, tresh = cv2.threshold(diff, mint, maxt, cv2.THRESH_BINARY)


        #first erode to remove noise
        tresh = cv2.erode(tresh,st, iterations=1)
        #then dilate to remove holes
        tresh = cv2.dilate(tresh, st, iterations=dilatiter)
        tresh = cv2.erode(tresh, st, iterations=eroditer)

        _, contours, hierarchy = cv2.findContours(tresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        #draw a boundingbox if something is moving
        drawBoundingBox(contours, frame1)

        #save image
        curtime = time.time()
        faces = face_cascade.detectMultiScale(gray1, 1.3, 5)  #returns an array with coordinates of rectangles around the found faces
        #numPictures = drawFaceBox(faces, frame1, gray1, numPictures, saveImage)
        numPictures, idface = faceRecognition(faces, frame1, gray1, numPictures, saveImage, recognizer)

        if saveImage == True:
            print("Number of pictures: " + str(numPictures))

        if numPictures >= 10:
            numPictures = 0
            ID += 1

        # detection status to screen
        if (len(contours) > 0):
            state = 'motion detected'
        else:
            state = ' '

        if (len(faces) > 0):
            face_state = 'face detected'
        else:
            face_state = ' '

        if idface < 0:
            idface = '      '

        #write the state "motion detected" to the screen
        cv2.putText(blank, state, (10, 50), cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),2,cv2.LINE_AA)
        cv2.putText(blank, face_state, (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.putText(blank, str(idface), (10, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        #showing images
        cv2.imshow('track', blank)
        cv2.imshow('origin', frame1)
        cv2.imshow('diff', diff)
        cv2.imshow('tresholded', tresh)

        # delete text from the screen for the next status
        cv2.putText(blank, state, (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
        cv2.putText(blank, face_state, (10, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)
        cv2.putText(blank, str(idface), (10, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 4, cv2.LINE_AA)


        if(debug == 1):
           #cv2.imshow('gray1', gray1)
           # cv2.imshow('hist', equ1)
            if(debug != prevDebug):
                createTrackbars('track')
        else:
           # cv2.destroyWindow('gray1')
            #cv2.destroyWindow('hist')
            if (debug != prevDebug):
                cv2.destroyWindow('track')

        prevDebug = debug


        # wait for keypress
        k = cv2.waitKey(60) & 0xff
        if k == 27:
            #if escape close app
            break
        elif k == ord('d'):
            if debug == 1:
                debug = 0
            else:
                debug = 1
        elif k == ord('s'):
            if saveImage == True:
                saveImage = False
                print("saving off")
            else:
                saveImage = True
                print('saving on')
        if cikltime - prevcikltime > 1:
          #  print(i)
            i = 0
            prevcikltime = cikltime

        cikltime = time.time()


    #release camerastream and close all windows
    cap.release();
    cv2.destroyAllWindows()

    #save back the new ID
    print("Saving back ID...")
    saveId(ID, path)

    print('Ended\n')
    return

#########################################################
#                   MAIN
#########################################################

#cascadePath = '/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml'
#recognizerpath = "/home/mate/PycharmProjects/open_cv/HomeWork/trainner/trainner.yml"
#securityCam(1, recognizerpath,cascadePath)
