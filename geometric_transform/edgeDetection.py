import cv2
import numpy as np
from matplotlib import pyplot as plt

def nothing(x):
    pass

cv2.namedWindow('origin',cv2.WINDOW_NORMAL)
cv2.namedWindow('edged', cv2.WINDOW_NORMAL)
img = cv2.imread('/home/mate/PycharmProjects/opencv/messi5.jpg',0)

cv2.createTrackbar('min','origin',0,500,nothing)
cv2.createTrackbar('max','origin',0,500,nothing)



while True:
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

    min = cv2.getTrackbarPos('min', 'origin')
    max = cv2.getTrackbarPos('max', 'origin')

    edges = cv2.Canny(img, min, max)

    cv2.imshow('origin', img)
    cv2.imshow('edged', edges)

cv2.destroyAllWindows()