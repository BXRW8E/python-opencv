import cv2
import numpy as np
from matplotlib import pyplot as plt

def nothing(x):
    pass


#create two window
cv2.namedWindow('Origin',cv2.WINDOW_NORMAL)
cv2.namedWindow('Edged',cv2.WINDOW_NORMAL)

cap = cv2.VideoCapture(0)

cv2.createTrackbar('min','Origin',0,500, nothing)
cv2.createTrackbar('max', 'Origin', 0,500, nothing)

while True:
    _, origin = cap.read()

    min = cv2.getTrackbarPos('min', 'Origin')
    max = cv2.getTrackbarPos('max', 'Origin')

    edged = cv2.GaussianBlur(origin, (5, 5), 0) #filtering
    edged = cv2.Canny(edged, min, max)

    #edged = origin
    cv2.imshow('Origin', origin)
    cv2.imshow('Edged', edged)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()