import cv2
import numpy as np
import time


drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
radius = 5
color = (0,0,255)

def changeColor(x):
    pass


# mouse callback function
def draw_circle(event,x,y,flags,param):
    global ix,iy,drawing,mode

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv2.rectangle(img, (ix, iy), (x, y), color, -1)
            else:
                cv2.circle(img, (x, y), radius, color, -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv2.rectangle(img, (ix, iy), (x, y), color, -1)
        else:
            cv2.circle(img, (x, y), radius, color, -1)


img = np.zeros((720,720,3), np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle)


# create trackbars for color change
cv2.createTrackbar('R','image',0,255,changeColor)
cv2.createTrackbar('G','image',0,255,changeColor)
cv2.createTrackbar('B','image',0,255,changeColor)
cv2.createTrackbar('Size', 'image', 0,100,changeColor)

while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('s'):
        times = str(time.time())
        times = times[0:9] + times[11:17]
        picName = "paint" + times + ".jpg"
        print(picName)
        cv2.imwrite(picName, img)
        #cv2.imwrite('paint.jpg',img)


        # get current positions of four trackbars
    r = cv2.getTrackbarPos('R', 'image')
    g = cv2.getTrackbarPos('G', 'image')
    b = cv2.getTrackbarPos('B', 'image')
    radius = cv2.getTrackbarPos('Size', 'image')
    color = (b, g, r)
cv2.destroyAllWindows()