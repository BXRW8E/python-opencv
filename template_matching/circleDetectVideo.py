
import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()


   # frame = cv2.medianBlur(frame, 5)

    cframe = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    circles = cv2.HoughCircles(cframe,cv2.HOUGH_GRADIENT,1,20,param1=100,param2=40,minRadius=0,maxRadius=0)

    circles = np.uint16(np.around(circles))

#print(circles)
    for i in circles[0,:]:
        # draw the outer circle
        cv2.circle(cframe,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle
        cv2.circle(cframe,(i[0],i[1]),2,(0,0,255),3)

    cv2.imshow('detected circles',cframe)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()