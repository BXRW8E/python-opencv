import cv2


cam = cv2.VideoCapture(0)
detector = cv2.CascadeClassifier('/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml')

Id = input('enter your id: ')

sampleNum = 0
cv2.namedWindow("img")
while (True):
    ret, img = cam.read()
    if not ret:
        continue

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = detector.detectMultiScale(gray, 1.3, 5)
    #cv2.imshow("img", img)
    #print(faces)
    if len(faces) != 1:
        cv2.imshow("img", img)
        print("len faces: " + str(len(faces)))
    else:
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            if(cv2.waitKey(1) & 0xFF == ord('n')):
                # incrementing sample number
                sampleNum = sampleNum + 1
                # saving the captured face in the dataset folder
                cv2.imwrite("dataSet/User." + Id + '.' + str(sampleNum) + ".jpg", gray[y:y + h, x:x + w])

            print(sampleNum)
        # wait for 100 miliseconds
        # break if the sample number is morethan 20
        cv2.imshow("img", img)

    if sampleNum > 10:
        break

cam.release()
cv2.destroyAllWindows()