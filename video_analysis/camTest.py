import cv2

cap = cv2.VideoCapture(0)

while True:
    flag, frame = cap.read()

    cv2.imshow('frame', frame)

    k = cv2.waitKey(10) & 0xFF
    if(k == ord('q')):
        break

cap.release()
cv2.destroyAllWindows()