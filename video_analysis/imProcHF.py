#!/usr/bin/env/python3

import numpy as np
import cv2
import datetime
import time
import imutils

def nothing(x):
    pass

def motionDetection():
    print("Starting...")
    #kernel for dilate and erode
    kernel = np.ones((3,3), np.uint8)
    fgbg = cv2.createBackgroundSubtractorMOG2()
    debug = 0

    #load face and eye cascades
    #face_cascade = cv2.CascadeClassifier('/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml')
    #eye_cascade = cv2.CascadeClassifier('/home/mate/opencv/data/haarcascadeshaarcascade_eye.xml')

    #open camstream
    cap = cv2.VideoCapture(0)
    time.sleep(0.25)
    # take first frame of the video
    ret,frame = cap.read()

    diff = cap.read()

    #create trackbar
    cv2.namedWindow('track')
    cv2.createTrackbar('min','track',0,255,nothing)
    cv2.createTrackbar('max','track',0,255,nothing)
    cv2.createTrackbar('erode','track', 0,100, nothing)
    cv2.createTrackbar('dilate', 'track', 0,100, nothing)
    cv2.createTrackbar('canny_min', 'track', 0, 255, nothing)
    cv2.createTrackbar('canny_max', 'track', 0, 255, nothing)
    #cv2.createTrackbar('xhuman', 'track', 0, 480, nothing)
    text = 'not detected'
    mint = 25
    maxt = 255
    cv2.setTrackbarPos('max', 'track', 255)
    cv2.setTrackbarPos('min', 'track', 15)
    cv2.setTrackbarPos('erode', 'track', 36)
    cv2.setTrackbarPos('dilate', 'track', 51)
    cannyMin = 0
    cannyMax = 0
    print(type(cap))
    while(1):
        #read the first frame
        ret1, frame1 = cap.read()
        if(not ret1):
            print("Could not read frame1\n")
            continue
        #resize image
        if(debug == 1):
            cv2.imshow('first',frame1)
        else:
            cv2.destroyWindow('first')

        #readed frame to grayscale
        gray1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)


        gray1 = cv2.GaussianBlur(gray1, (21, 21), 0)
        frame1 = imutils.resize(frame1, width=640)



      # frame1 = cv2.equalizeHist(frame1)
       # gray1 = cv2.equalizeHist(gray1)

        #read the second frame. frame1 + dt
        ret2, frame2 = cap.read()
        if (not ret2):
            print("Could not read frame2\n")
            continue
        #frame2 to grayscale and blur
        gray2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
        gray2 = cv2.GaussianBlur(gray2, (21, 21), 0)
      #  gray2 = cv2.equalizeHist(gray2)

        #difference image
        diff = cv2.absdiff(gray1, gray2);

        #trackbar position set to default, it can be changed
        mint = cv2.getTrackbarPos('min', 'track')
        maxt = cv2.getTrackbarPos('max', 'track')
        erod = cv2.getTrackbarPos('erode', 'track')
        dilat = cv2.getTrackbarPos('dilate', 'track')
        cannyMin = cv2.getTrackbarPos('canny_min','track')
        cannyMax = cv2.getTrackbarPos('canny_max','track')

        #treshold the differential image
        ret, thresh = cv2.threshold(diff, mint, maxt, cv2.THRESH_BINARY)

        #thresh = cv2.erode(thresh, kernel, iterations=1)
        #dilate the tresholded image
        thresh = cv2.dilate(thresh, None, iterations=dilat)
        thresh = cv2.erode(thresh, None, iterations=erod)
       # thresh = cv2.GaussianBlur(thresh, (21,21), 0)
        #thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
        _, contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
      #  print(str(len(contours))+'\n')
        if(len(contours) > 0):
            print('detected')
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if(h < 70 or w < 60):
                continue
            print('width: ' + str(w) + '  height: ' + str(h) + '\n')
            cv2.rectangle(frame1, (x, y), (x + w, y + h), (255, 0, 0), 2)
            if(h > w ):
                print('human\n')
                print(str(x) + '\n')
            else:
                print('animal\n')


        #write datetime to the origin window
        cv2.putText(frame1, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                    (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        #showing the frames
        cv2.imshow('Origin', frame1)
        cv2.imshow("Difference", diff)
        cv2.imshow("Tresholded", thresh)

        if debug == 1:
            # show the grayscale image
            # background remove
            fgmask = fgbg.apply(frame1)
            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)

            cv2.imshow('subtractor', fgmask)
        else:
           # cv2.destroyWindow('image')
            cv2.destroyWindow('subtractor')

        #wait for keypress
        k = cv2.waitKey(60) & 0xff
        if k == 27:
            break
        elif k == ord('d'):
            if debug == 1:
                debug = 0
            else:
                debug = 1

    #close windows and release camera
    cv2.destroyAllWindows()
    cap.release()
    print("Ended")
    return


#the functioncall
motionDetection()