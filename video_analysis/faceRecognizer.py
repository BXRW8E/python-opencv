import cv2
import numpy as np

recognizer = cv2.face.createLBPHFaceRecognizer()
recognizer.load('trainner/trainner.yml')
cascadePath = "/home/mate/opencv/data/haarcascades/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath);

prevId = 100
cam = cv2.VideoCapture(0)
#font = cv2.InitFont(cv2.CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 1, 1)
while True:
    ret, im = cam.read()
    gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(gray, 1.2,5)
    for(x,y,w,h) in faces:
        cv2.rectangle(im,(x,y),(x+w,y+h),(225,0,0),2)
        Id, conf = recognizer.predict(gray[y:y+h,x:x+w])
        print(conf)
        if(conf < 110):
            if(Id == 1):
                name = "Mate"
                #print("mate")
            elif(Id == 2):
                name = "Kovbal"
                #print("Kovbal")
            elif(Id == 3):
                name = "Obama"
            elif(Id == 4):
                name = "Kitti"
        else:
            name = "Unknown"
        #else:
        #   Id="Unknown"
        #cv2.PutText(cv2.fromarray(im),str(Id), (x,y+h),font, 255)
        print(name)
    cv2.imshow('im',im)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break
cam.release()
cv2.destroyAllWindows()

def faceRecognition(inputPicture, recognizerPath, cascadePath):
    """ it returns  the Id of a person if he was recognized
     inputPicture is the picture, that must be checked
     recognizerPath is the path of the .yml file
     cascadePath is the path of default haarcascade_frontalface_default.xml"""
    Id = -1    # this is the Id of an person
    recognizer = cv2.face.createLBPHFaceRecognizer()
    recognizer.load(recognizerPath)
    faceCascade = cv2.CascadeClassifier(cascadePath)

    gray = cv2.cvtColor(inputPicture, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(gray, 1.2, 5)

    for (x, y, w, h) in faces:
        cv2.rectangle(im, (x, y), (x + w, y + h), (225, 0, 0), 2)
        Id, conf = recognizer.predict(gray[y:y + h, x:x + w])
        if (conf > 110):
            Id = -1

    return Id