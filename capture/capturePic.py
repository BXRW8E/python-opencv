import numpy as np
import cv2
import os.path
import time

cap = cv2.VideoCapture(-1)
no = 0    #szamolja a kepeket

def writeLastPicNameToFile(lastPicName):
    f = open('picLogger.txt', 'w')   #megynitja a fajlt amiben az utolso nev van eltarolva
    s = str(lastPicName)
    f.write(s)
    f.close()


picName = "output.jpg"
while (cap.isOpened()):
    ##times = str(time.time())
    ##times = times[0:9]
    ##print(times)
    ret, frame = cap.read()
    if ret == True:
       # frame = cv2.flip(frame, 0)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        elif cv2.waitKey(1) == ord('c'):
            times = str(time.time())
            times = times[0:9] + times[11:17]
            picName = "output" + times +".jpg"
            print(picName)
            cv2.imwrite(picName, frame)

    else:
        break

cap.release()
cv2.destroyAllWindows()