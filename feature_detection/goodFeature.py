import numpy as np
import cv2
from matplotlib import pyplot as plt

file = '/home/mate/PycharmProjects/opencv/Free-Shipping-LEGO-Shop.jpg'
img = cv2.imread(file)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#cv2.goodFeaturesToTrack()
corners = cv2.goodFeaturesToTrack(gray,30,0.01,10)
corners = np.int0(corners)

for i in corners:
    x,y = i.ravel()
    cv2.circle(img,(x,y),3,255,-1)

plt.imshow(img),plt.show()