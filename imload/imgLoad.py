#loads an image and show it up.
# if 's' pressed saves it
# if 'esc' pressed then close it
#"/home/mate/Pictures/google-pics/20160313_130537.jpg"

import numpy as np
import cv2

#cv2.IMREAD_COLOR
img = cv2.imread("/home/mate/Pictures/google-pics/20160313_130537.jpg",cv2.IMREAD_GRAYSCALE)
#img = cv2.IMREAD_COLOR("/home/mate/Pictures/google-pics/20160313_130537.jpg",0)
cv2.namedWindow('image', cv2.WINDOW_NORMAL) #resizable window
cv2.imshow('image',img)
print("OpenCV Version: {}". format(cv2. __version__))
k = cv2.waitKey(0) & 0xFF
if k == 27:
    cv2.destroyAllWindows()
elif k == ord('s'):
    cv2.imwrite("/home/mate/Downloads/temp/feketefeher.jpg", img)