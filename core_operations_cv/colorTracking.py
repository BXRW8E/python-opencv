import cv2
import numpy as np

def nothing(x):
    pass

cv2.namedWindow('frame')
cap = cv2.VideoCapture(0)

# create trackbars for color change
cv2.createTrackbar('Hue','frame',0,360,nothing)
cv2.createTrackbar('Saturation','frame',0,255,nothing)
cv2.createTrackbar('Brightness','frame',0,255,nothing)
cv2.createTrackbar('Hueintervall', 'frame', 0,100, nothing)
cv2.createTrackbar('Brightness intervall', 'frame', 0, 100, nothing)

while(1):
# Take each frame
    _, frame = cap.read()


    h = cv2.getTrackbarPos('Hue','frame')
    s = cv2.getTrackbarPos('Saturation','frame')
    v = cv2.getTrackbarPos('Brightness','frame')
    border = cv2.getTrackbarPos('Hueintervall', 'frame')
    intr = cv2.getTrackbarPos('Brightness intervall', 'frame')

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([h-border, s-intr, v-intr])
    upper_blue = np.array([h+border, s+intr, v+intr])
    #lower_blue = np.array([110, 50, 50])
    #upper_blue = np.array([130, 255, 255])


    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower_blue, upper_blue)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()